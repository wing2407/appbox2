appbox2(appbox完全重构版)


重构后有以下需要注意的地方

==============================================================================================

接口方面的改动：

广告列表：

由原来的
{"success":true,"data":[{"_id":"55e56add1d41c8039dd1ce00","name":"balabala","packagename":"balabala"},{"_id":"66e56add1d41c8039dd1ce02","name":"balabala","packagename":"balabala"}]}

改为(ver是保存到app数据库的表名，app根据表名确定是否更换广告数据，data里面都是各个app接口的_id，获取后会根据id访问各app的资源数据)
{"success":true,"ver":"L151010","data":["55e56add1d41c8039dd1ce00","66e56add1d41c8039dd1ce02","77e56add1d41c8039dd1ce04"]}

--------------------------------------------------------------------------
app的资源数据网址分别在

UpdataIntentService.class中onHandleIntent方法里的相对应的对象里
广告列表：String url
app_info: Request request_info
app_icon: Request request_icon

GridFragment.class中downAndInstall方法的对象
app_download： HttpHandler handler


=================================================================================================

TaskService变动：

加入type：GetUserIn 功能：可提供用户安装，更新，卸载的app列表

静默安装后直接post cmd的结果返回，不再使用/v1/app/install接口，并且静默不成功不会弹出安装框

===================================================================================================

原来：
用户点击广告列表中的图标-->下载-->弹出安装框（没有后续了）

现在：
用户点击广告列表中的图标-->下载-->弹出安装框-->成功安装则直接打开广告app（失败不做其他操作）

====================================================================================================


重构升级的部分：
1、性能方面更好了，不像之前的app有明显卡滞的感觉
2、界面会根据数据量的大小自动生成对应个数的Fragment（UI）
3、原来app的downloadjson服务是每隔600秒访问一次广告接口，现在是只在开启app的时候获取一次就存到数据库，列表有更新时候再获取
4、广告列表变成一个可滑动的列表，点击换一批按钮相当于手动滑动，而且广告列表的容量大大提升了（不仅仅是4个，接口数据有几个就能显示几个）

